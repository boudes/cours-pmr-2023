package fr.mipn.exo03

type Euro = Int
type Id = Int 

sealed trait Message
case class Commande(conso: String, table: Id, quantité: Int) extends Message
case class Addition(table: Id) extends Message
case object Merci extends Message

def additions(prix: Map[String, Euro], msgs : List[Message]): List[(Id, Euro)] =
    msgs.foldLeft((List(): List[(Id, Euro)], Map(): Map[Id, Euro])) {
       case ((adds, addspart), msg) => msg match 
        case Commande(conso, table, quantité) =>
          (adds, addspart.updatedWith(table)({
            case Some(add) => Some(add + prix(conso) * quantité)
            case None => Some(prix(conso) * quantité) 
          }))
        case Addition(table) => 
          ((table, addspart.getOrElse(table,0)) :: adds, addspart.removed(table))
        case Merci => (adds, addspart) 
    }._1.reverse
 
@main def hello: Unit =
  println("Hello EXO 03!")
  val prix = Map("café" -> 3, "thé" -> 12, "jus d'orange" -> 5)
  val commandes = List(
    Commande("café", 3, 2),
    Commande("thé", 3, 1),
    Commande("café", 2, 1),
    Addition(2), // 3
    Merci,
    Commande("café", 2, 1),
    Merci,
    Commande("thé", 3, 1),
    Addition(3), // 
    Addition(2), 
  )
  additions(
    prix, 
    commandes).foreach(println)
